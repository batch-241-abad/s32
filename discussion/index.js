let http = require('http');
let port = 3000;
const server = http.createServer(function(request, response){

	if(request.url == '/items' && request.method == 'GET'){
		response.writeHead(201, {'Content-Type': 'text/plain'});
		response.end('Data retrieved from the database')
	}else if(request.url == '/items' && request.method == 'POST'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data was created and sent to the database')
	}
})
server.listen(port);
console.log("server is now "+port);